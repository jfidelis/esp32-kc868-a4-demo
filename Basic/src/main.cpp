#include <Arduino.h>
#include "board_defs.h"

void setup() {
    pinMode(RELAY1, OUTPUT);
    pinMode(RELAY2, OUTPUT);
    pinMode(RELAY3, OUTPUT);
    pinMode(RELAY4, OUTPUT);
    pinMode(BEEP, OUTPUT);
}

void loop() {
    digitalWrite(RELAY1, HIGH);
    delay(5000);
    digitalWrite(RELAY2, HIGH);
    delay(5000);
    digitalWrite(RELAY3, HIGH);
    delay(5000);
    digitalWrite(RELAY4, HIGH);
    delay(5000);   
    digitalWrite(RELAY1, LOW);
    delay(5000);
    digitalWrite(RELAY2, LOW);
    delay(5000);
    digitalWrite(RELAY3, LOW);
    delay(5000);
    digitalWrite(RELAY4, LOW);   
    delay(5000);   
    //digitalWrite(BEEP, HIGH);
    //delay(1000);
    //digitalWrite(BEEP, LOW);  
}