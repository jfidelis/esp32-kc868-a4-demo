#include <Arduino.h>
#include "board_defs.h"
#include <IRremote.h>

void setup() {
    pinMode(RELAY1, OUTPUT);
    pinMode(RELAY2, OUTPUT);
    pinMode(RELAY3, OUTPUT);
    pinMode(RELAY4, OUTPUT);

    Serial.begin(115200);
    // Just to know which program is running on my Arduino
    Serial.println(F("START " __FILE__ " from " __DATE__ "\r\nUsing library version " VERSION_IRREMOTE));

    /*
     * Start the receiver, enable feedback LED and take LED feedback pin from the internal boards definition
     */
    IrReceiver.begin(IR_RECEIVE_PIN, ENABLE_LED_FEEDBACK, USE_DEFAULT_FEEDBACK_LED_PIN);

    Serial.print(F("Ready to receive IR signals at pin "));
    Serial.println(IR_RECEIVE_PIN);      
}

void loop() {
    if (IrReceiver.decode()) {

        // Print a short summary of received data
        IrReceiver.printIRResultShort(&Serial);
        if (IrReceiver.decodedIRData.protocol == UNKNOWN) {
            // We have an unknown protocol here, print more info
            IrReceiver.printIRResultRawFormatted(&Serial, true);
        }
        Serial.println();

        /*
         * !!!Important!!! Enable receiving of the next value,
         * since receiving has stopped after the end of the current received data packet.
         */
        IrReceiver.resume(); // Enable receiving of the next value

        if (IrReceiver.decodedIRData.command == 0x11) { 
            // Rele 1
            digitalWrite(RELAY1, HIGH);
            delay(3000);
            digitalWrite(RELAY1, LOW);
        } 
        else 
        if (IrReceiver.decodedIRData.command == 0xE) {
            // Rele 2
            digitalWrite(RELAY2, HIGH);
            delay(3000);
            digitalWrite(RELAY2, LOW);            
        }
        else 
        if (IrReceiver.decodedIRData.command == 0xF) {
            // Rele 3
            digitalWrite(RELAY3, HIGH);
            delay(3000);
            digitalWrite(RELAY3, LOW);            
        }
        else 
        if (IrReceiver.decodedIRData.command == 0x15) {
            // Rele 4
            digitalWrite(RELAY4, HIGH);
            delay(3000);
            digitalWrite(RELAY4, LOW);            
        }                
    }
}