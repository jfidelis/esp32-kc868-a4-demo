#define DAC1 25
#define DAC2 26

#define ANALOG_A3 32
#define ANALOG_A4 33

#define ANALOG_A1 34
#define ANALOG_A2 35

#define RELAY1 2
#define RELAY2 15
#define RELAY3 5
#define RELAY4 4

#define DI1 36
#define DI2 39
#define DI3 27
#define DI4 14

#define BEEP 18

// DS18B20 ds(13)

#define IR_RECEIVE_PIN 23
#define IR_SEND_PIN 22
#define tone(a,b,c) void()     
#define noTone(a) void()
#define TONE_PIN    42 // Dummy for examples using it
#define APPLICATION_PIN 0
